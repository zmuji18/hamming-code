#include "data.h"
#include "hamming_assert.h"
#include "bits.h"

#include <stddef.h>
#include <malloc.h>
#include <memory.h>


#define is_initialized(data) (data->bytes != NULL)

typedef struct Data {
    uint8_t *bytes;     // The bytes of data stored in this structure.
    uint32_t n_bytes;   // The number of data bytes stored in this structure.
    uint32_t n_bits;    // The number of bits logically stored in this structure.
} Data;

Data *data_create() {

    Data *data = malloc(sizeof(Data));
    data->bytes = NULL;

    return data;
}

Data *data_create_from(const uint8_t *data_bytes, uint32_t n_bytes, uint32_t n_bits) {

    Data *data = data_create();
    data_initialize(data, data_bytes, n_bytes, n_bits);

    return data;
}

Data *data_create_wrap_from(uint8_t *data_bytes, uint32_t n_bytes, uint32_t n_bits) {

    Data *data = data_create();
    data_wrap(data, data_bytes, n_bytes, n_bits);

    return data;
}

void data_initialize(Data *data, const uint8_t *data_bytes, uint32_t n_bytes, uint32_t n_bits) {

    ASSERT((n_bytes != 0) ^ (n_bits != 0)); // The size of the data should be specified in either Bytes or Bits.

    uint32_t bytes_to_copy = (n_bytes == 0 ? (n_bits - 1) / BITS_IN_BYTE + 1 : n_bytes);

    uint8_t *copied_data = malloc(sizeof(uint8_t) * bytes_to_copy);
    memcpy(copied_data, data_bytes, sizeof(uint8_t) * bytes_to_copy);

    data_wrap(data, copied_data, n_bytes, n_bits);
}

void data_wrap(Data *data, uint8_t *data_bytes, uint32_t n_bytes, uint32_t n_bits) {

    ASSERT(data_bytes != NULL);
    ASSERT(!is_initialized(data));          // The same Data cannot be initialized twice.
    ASSERT((n_bytes != 0) ^ (n_bits != 0)); // The size of the data should be specified in either Bytes or Bits.

    if (n_bytes == 0) {
        n_bytes = (n_bits - 1) / BITS_IN_BYTE + 1;
    }

    if (n_bits == 0) {
        n_bits = n_bytes * BITS_IN_BYTE;
    }

    data->bytes = data_bytes;
    data->n_bytes = n_bytes;
    data->n_bits = n_bits;
}


const uint8_t *get_bytes(const Data *data) {
    ASSERT(data != NULL);
    ASSERT(is_initialized(data));
    return data->bytes;
}

uint32_t get_byte_count(const Data *data) {
    ASSERT(data != NULL);
    ASSERT(is_initialized(data));
    return data->n_bytes;
}

uint32_t get_bit_count(const Data *data) {
    ASSERT(data != NULL);
    ASSERT(is_initialized(data));
    return data->n_bits;
}

void data_dispose(Data *data) {

    ASSERT(data != NULL);

    if (data->bytes != NULL) {
        free(data->bytes);
    }
    data_dispose_wrap(data);
}

void data_dispose_wrap(Data *data) {
    free(data);
}