#ifndef __HAMMING_CODE_BITS_H__
#define __HAMMING_CODE_BITS_H__

#define BITS_IN_BYTE 8

/**
 * Evaluates to true if the specified number is a pure power of two.
 */
#define is_power_of_two(number) (((number) & -(number)) == (number) && (number) > 0)

/**
 * Evaluates to the index of the specified MSB bit.
 */
#define msb_index(index) (BITS_IN_BYTE - (index) - 1)

/**
 * Converts any integer number to a boolean value (1 or 0)
 */
#define to_bit(number) ((number) ? 1 : 0)

/**
 * Evaluates the bit at the specified index for the given integer.
 */
#define get_bit(number, bit_index) to_bit((number) & (1 << (bit_index)))

/**
 * Evaluates the bit at specified index from the specified sequence of bytes. Indexing
 * starts from the MSB of the first byte.
 */
#define get_array_bit_msb(base, bit_index)                                              \
    get_bit(base[(bit_index) / BITS_IN_BYTE], msb_index((bit_index) % BITS_IN_BYTE))    \

/**
 * Sets the specified bit for the given variable.
 */
#define set_one_bit(var, bit_index) var |= (1 << (bit_index))

/**
 * Resets the specified bit for the given variable.
 */
#define set_zero_bit(var, bit_index) var &= ~(1 << (bit_index))

/**
 * Sets the specified bit for the variable to the given value.
 */
#define set_bit(var, bit_index, bit) {if(bit) set_one_bit(var, bit_index); else set_zero_bit(var, bit_index);}

/**
 * Sets the bit at specified index from the specified sequence of bytes to the given value. Indexing
 * starts from the MSB of the first byte.
 */
#define set_array_bit_msb(base, bit_index, bit)                                             \
    set_bit(base[(bit_index) / BITS_IN_BYTE], msb_index((bit_index) % BITS_IN_BYTE), bit)   \

#endif /* __HAMMING_CODE_BITS_H__ */
