#ifndef __HAMMING_CODE_ASSERT_H__
#define __HAMMING_CODE_ASSERT_H__

#include <assert.h>

#ifndef ASSERT
#define ASSERT(condition) assert(condition)
#endif


#endif /* __HAMMING_CODE_ASSERT_H__ */
