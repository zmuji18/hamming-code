#include "unity.h"
#include "data.h"

#define BITS_IN_BYTE 8

typedef struct {
    const uint8_t *expected_bytes;
    uint32_t expected_n_bytes;
    uint32_t expected_n_bits;
} TestResult;

static Data *output;
static TestResult expected;

static void expect(const uint8_t *expected_bytes, uint32_t expected_n_bytes, uint32_t expected_n_bits) {
    expected.expected_bytes = expected_bytes;
    expected.expected_n_bytes = expected_n_bytes;
    expected.expected_n_bits = expected_n_bits;
}

static void given(Data *data) {

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, data, "The created data must not be null.");
    TEST_ASSERT_EQUAL(expected.expected_n_bytes, get_byte_count(data));
    TEST_ASSERT_EQUAL(expected.expected_n_bits, get_bit_count(data));

    // All bytes until the last should be exactly the same.
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected.expected_bytes, get_bytes(data),
                                  expected.expected_n_bytes - 1);

    uint8_t last_byte = expected.expected_bytes[expected.expected_n_bytes - 1] &
                        ~(1 << (expected.expected_n_bits % BITS_IN_BYTE));

    // Only the bits that are part of the data need to be the same (the rest may not).
    TEST_ASSERT_EQUAL(last_byte, get_bytes(data)[expected.expected_n_bytes - 1]);
}

void setUp(void) {
    output = data_create();
}

void tearDown(void) {
    data_dispose(output);
}

void test_should_create_data(void) {
    Data *test_data = data_create();
    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, test_data, "The created data must not be null.");
    data_dispose(test_data);
}

void test_should_dispose_initialized_data(void) {

    Data *test_data = data_create();

    uint8_t test_bytes[] = {1, 2, 3, 4, 5};
    data_initialize(test_data, test_bytes, 5, 0);

    data_dispose(test_data);
}

void test_should_dispose_initialized_wrap_data(void) {

    Data *test_data = data_create();

    uint8_t test_bytes[] = {1, 2, 3, 4, 5};
    data_wrap(test_data, test_bytes, 5, 0);

    data_dispose_wrap(test_data);
}

void test_should_dispose_uninitialized_data(void) {
    Data *test_data = data_create();
    data_dispose(test_data);
}

void test_should_create_data_from_bytes(void) {

    #undef N_BYTES
    #undef N_BITS
    #define N_BYTES 10
    #define N_BITS 80

    const uint8_t test_bytes[N_BYTES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    output = data_create_from(test_bytes, N_BYTES, 0);

    expect(test_bytes, N_BYTES, N_BITS);
    given(output);
}

void test_should_create_wrap_data_from_bits(void) {

    #undef N_BYTES
    #undef N_BITS
    #define N_BYTES 10
    #define N_BITS 80

    uint8_t test_bytes[N_BYTES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    Data *wrap_output = data_create_wrap_from(test_bytes, 0, N_BITS);

    expect(test_bytes, N_BYTES, N_BITS);
    given(wrap_output);

    data_dispose_wrap(wrap_output);
}

void test_should_initialize_data_with_bytes(void) {

    #undef N_BYTES
    #undef N_BITS
    #define N_BYTES 10
    #define N_BITS 80

    const uint8_t test_bytes[N_BYTES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    data_initialize(output, test_bytes, N_BYTES, 0);

    expect(test_bytes, N_BYTES, N_BITS);
    given(output);
}

void test_should_wrap_data_with_bits(void) {

    #undef N_BYTES
    #undef N_BITS
    #define N_BYTES 10
    #define N_BITS 80

    uint8_t test_bytes[N_BYTES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    Data *new_data = data_create();
    data_wrap(new_data, test_bytes, N_BYTES, 0);

    expect(test_bytes, N_BYTES, N_BITS);
    given(new_data);

    data_dispose_wrap(new_data);
}

void test_should_initialize_data_with_bits(void) {

    #undef N_BYTES
    #undef N_BITS
    #define N_BYTES 10
    #define N_BITS 80

    uint8_t test_bytes[N_BYTES] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    data_initialize(output, test_bytes, 0, N_BITS);

    expect(test_bytes, N_BYTES, N_BITS);
    given(output);
}

void test_should_initialize_data_with_incomplete_byte(void) {

    #undef N_BITS
    #undef N_BYTES
    #define N_BYTES 5
    #define N_BITS 37

    uint8_t test_bytes[N_BYTES] = {1, 2, 3, 4, 5};
    data_initialize(output, test_bytes, 0, N_BITS);

    expect(test_bytes, N_BYTES, N_BITS);
    given(output);
}
