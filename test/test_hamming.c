#include "unity.h"
#include "data.h"
#include "hamming.c"

Data *output;

void setUp(void) {
    output = data_create();
}

void tearDown(void) {
    data_dispose(output);
}

void test_should_encode_aligned_data_to_aligned_packet(void) {

    #undef DATA_BIT_OFFSET
    #undef N_DATA_BITS
    #undef N_BITS_PER_PACKET
    #undef ENCODED_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define DATA_BIT_OFFSET 4
    #define N_DATA_BITS 4
    #define N_BITS_PER_PACKET 8
    #define ENCODED_BIT_OFFSET 0
    #define BYTES_TO_CHECK 1

    uint8_t test_bytes[] = {0b00001010};
    uint8_t expected_bytes[] = {0b01011010};

    uint8_t encoded_bytes[BYTES_TO_CHECK];

    encode_data_packet(test_bytes,
                       DATA_BIT_OFFSET,
                       N_DATA_BITS,
                       N_BITS_PER_PACKET,
                       encoded_bytes,
                       ENCODED_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, encoded_bytes, BYTES_TO_CHECK);
}

void test_should_encode_misaligned_data_to_aligned_packet(void) {

    #undef DATA_BIT_OFFSET
    #undef N_DATA_BITS
    #undef N_BITS_PER_PACKET
    #undef ENCODED_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define DATA_BIT_OFFSET 6
    #define N_DATA_BITS 4
    #define N_BITS_PER_PACKET 8
    #define ENCODED_BIT_OFFSET 0
    #define BYTES_TO_CHECK 1

    uint8_t test_bytes[] = {0b00000010, 0b10000000};
    uint8_t expected_bytes[] = {0b01011010};

    uint8_t encoded_bytes[BYTES_TO_CHECK];

    encode_data_packet(test_bytes,
                       DATA_BIT_OFFSET,
                       N_DATA_BITS,
                       N_BITS_PER_PACKET,
                       encoded_bytes,
                       ENCODED_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, encoded_bytes, BYTES_TO_CHECK);
}

void test_should_encode_aligned_data_to_misaligned_packet(void) {

    #undef DATA_BIT_OFFSET
    #undef N_DATA_BITS
    #undef N_BITS_PER_PACKET
    #undef ENCODED_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define DATA_BIT_OFFSET 4
    #define N_DATA_BITS 4
    #define N_BITS_PER_PACKET 8
    #define ENCODED_BIT_OFFSET 4
    #define BYTES_TO_CHECK 2

    uint8_t test_bytes[] = {0b00001010};
    uint8_t expected_bytes[] = {0b00000101, 0b10100000};

    uint8_t encoded_bytes[BYTES_TO_CHECK] = {0};

    encode_data_packet(test_bytes,
                       DATA_BIT_OFFSET,
                       N_DATA_BITS,
                       N_BITS_PER_PACKET,
                       encoded_bytes,
                       ENCODED_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, encoded_bytes, BYTES_TO_CHECK);
}

void test_should_encode_misaligned_data_to_misaligned_packet(void) {

    #undef DATA_BIT_OFFSET
    #undef N_DATA_BITS
    #undef N_BITS_PER_PACKET
    #undef ENCODED_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define DATA_BIT_OFFSET 6
    #define N_DATA_BITS 4
    #define N_BITS_PER_PACKET 8
    #define ENCODED_BIT_OFFSET 4
    #define BYTES_TO_CHECK 2

    uint8_t test_bytes[] = {0b00000010, 0b10000000};
    uint8_t expected_bytes[] = {0b00000101, 0b10100000};

    uint8_t encoded_bytes[BYTES_TO_CHECK] = {0};

    encode_data_packet(test_bytes,
                       DATA_BIT_OFFSET,
                       N_DATA_BITS,
                       N_BITS_PER_PACKET,
                       encoded_bytes,
                       ENCODED_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, encoded_bytes, BYTES_TO_CHECK);
}

void test_should_encode_data_in_bytes(void) {

    #undef N_BYTES
    #undef N_BITS_PER_PACKET
    #undef N_PACKETS
    #undef N_EXPECTED_BYTES
    #undef N_EXPECTED_BITS
    #define N_BYTES 4
    #define N_BITS_PER_PACKET 8
    #define N_PACKETS 8
    #define N_EXPECTED_BYTES 8
    #define N_EXPECTED_BITS 64

    uint8_t test_bytes[N_BYTES] = {1, 2, 3, 4};
    uint8_t encoded_test_bytes[N_EXPECTED_BYTES] = {0, 0b01101001, 0, 0b10101010, 0, 0b11000011, 0, 0b11001100};

    data_initialize(output, test_bytes, N_BYTES, 0);

    uint32_t n_packets;
    Data *encoded_data = encode_data(output, N_BITS_PER_PACKET, &n_packets);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, encoded_data, "The encoded data should not be null.");
    TEST_ASSERT_EQUAL(N_PACKETS, n_packets);
    TEST_ASSERT_EQUAL(N_EXPECTED_BYTES, get_byte_count(encoded_data));
    TEST_ASSERT_EQUAL(N_EXPECTED_BITS, get_bit_count(encoded_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(encoded_test_bytes, get_bytes(encoded_data), n_packets);

    data_dispose(encoded_data);
}

void test_should_encode_data_in_bits(void) {

    #undef N_DATA_BITS
    #undef N_DATA_BYTES
    #undef N_BITS_PER_PACKET
    #undef N_PACKETS
    #undef N_EXPECTED_BYTES
    #undef N_EXPECTED_BITS
    #define N_DATA_BITS 28
    #define N_DATA_BYTES 4
    #define N_BITS_PER_PACKET 8
    #define N_PACKETS 7
    #define N_EXPECTED_BYTES 7
    #define N_EXPECTED_BITS 56

    uint8_t test_bytes[N_DATA_BYTES] = {1, 2, 3, 0b00000100};
    uint8_t encoded_test_bytes[N_EXPECTED_BYTES] = {0, 0b01101001, 0, 0b10101010, 0, 0b11000011, 0};

    data_initialize(output, test_bytes, 0, N_DATA_BITS);

    uint32_t n_packets;
    Data *encoded_data = encode_data(output, N_BITS_PER_PACKET, &n_packets);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, encoded_data, "The encoded data should not be null.");
    TEST_ASSERT_EQUAL(N_PACKETS, n_packets);
    TEST_ASSERT_EQUAL(N_EXPECTED_BYTES, get_byte_count(encoded_data));
    TEST_ASSERT_EQUAL(N_EXPECTED_BITS, get_bit_count(encoded_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(encoded_test_bytes, get_bytes(encoded_data), n_packets);

    data_dispose(encoded_data);
}

void test_should_decode_aligned_packet_to_aligned_data(void) {

    #undef ENCODED_BIT_OFFSET
    #undef N_BITS_PER_PACKET
    #undef DATA_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define ENCODED_BIT_OFFSET 0
    #define N_BITS_PER_PACKET 8
    #define DATA_BIT_OFFSET 4
    #define BYTES_TO_CHECK 1

    uint8_t test_bytes[] = {0b01011010};
    uint8_t expected_bytes[] = {0b00001010};

    uint8_t data_bytes[BYTES_TO_CHECK] = {0};

    decode_data_packet(test_bytes,
                       ENCODED_BIT_OFFSET,
                       N_BITS_PER_PACKET,
                       data_bytes,
                       DATA_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, data_bytes, BYTES_TO_CHECK);
}

void test_should_decode_aligned_packet_to_misaligned_data(void) {

    #undef ENCODED_BIT_OFFSET
    #undef N_BITS_PER_PACKET
    #undef DATA_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define ENCODED_BIT_OFFSET 0
    #define N_BITS_PER_PACKET 8
    #define DATA_BIT_OFFSET 6
    #define BYTES_TO_CHECK 2

    uint8_t test_bytes[] = {0b01011010};
    uint8_t expected_bytes[] = {0b00000010, 0b10000000};

    uint8_t data_bytes[BYTES_TO_CHECK] = {0};

    decode_data_packet(test_bytes,
                       ENCODED_BIT_OFFSET,
                       N_BITS_PER_PACKET,
                       data_bytes,
                       DATA_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, data_bytes, BYTES_TO_CHECK);
}

void test_should_decode_misaligned_packet_to_aligned_data(void) {

    #undef ENCODED_BIT_OFFSET
    #undef N_BITS_PER_PACKET
    #undef DATA_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define ENCODED_BIT_OFFSET 4
    #define N_BITS_PER_PACKET 8
    #define DATA_BIT_OFFSET 4
    #define BYTES_TO_CHECK 1

    uint8_t test_bytes[] = {0b00000101, 0b10100000};
    uint8_t expected_bytes[] = {0b00001010};

    uint8_t data_bytes[BYTES_TO_CHECK] = {0};

    decode_data_packet(test_bytes,
                       ENCODED_BIT_OFFSET,
                       N_BITS_PER_PACKET,
                       data_bytes,
                       DATA_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, data_bytes, BYTES_TO_CHECK);
}

void test_should_decode_misaligned_data_to_misaligned_packet(void) {

    #undef ENCODED_BIT_OFFSET
    #undef N_BITS_PER_PACKET
    #undef DATA_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define ENCODED_BIT_OFFSET 4
    #define N_BITS_PER_PACKET 8
    #define DATA_BIT_OFFSET 6
    #define BYTES_TO_CHECK 2

    uint8_t test_bytes[] = {0b00000101, 0b10100000};
    uint8_t expected_bytes[] = {0b00000010, 0b10000000};

    uint8_t data_bytes[BYTES_TO_CHECK] = {0};

    decode_data_packet(test_bytes,
                       ENCODED_BIT_OFFSET,
                       N_BITS_PER_PACKET,
                       data_bytes,
                       DATA_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, data_bytes, BYTES_TO_CHECK);
}

void test_should_decode_corrupted_packet(void) {

    #undef ENCODED_BIT_OFFSET
    #undef N_BITS_PER_PACKET
    #undef DATA_BIT_OFFSET
    #undef BYTES_TO_CHECK
    #define ENCODED_BIT_OFFSET 4
    #define N_BITS_PER_PACKET 8
    #define DATA_BIT_OFFSET 6
    #define BYTES_TO_CHECK 2

    // Uncorrupted Data:   {0b00000101, 0b10100000};
    uint8_t test_bytes[] = {0b00000100, 0b10100000};
    uint8_t expected_bytes[] = {0b00000010, 0b10000000};

    uint8_t data_bytes[BYTES_TO_CHECK] = {0};

    decode_data_packet(test_bytes,
                       ENCODED_BIT_OFFSET,
                       N_BITS_PER_PACKET,
                       data_bytes,
                       DATA_BIT_OFFSET);

    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, data_bytes, BYTES_TO_CHECK);
}

void test_should_decode_data_in_bytes(void) {

    #undef N_BITS_PER_PACKET
    #undef N_ENCODED_BYTES
    #undef N_EXPECTED_BYTES
    #undef N_EXPECTED_BITS
    #define N_BITS_PER_PACKET 8
    #define N_ENCODED_BYTES 8
    #define N_EXPECTED_BYTES 4
    #define N_EXPECTED_BITS 32

    uint8_t test_bytes[N_ENCODED_BYTES] = {0, 0b01101001, 0, 0b10101010, 0, 0b11000011, 0, 0b11001100};
    uint8_t decoded_test_bytes[N_EXPECTED_BYTES] = {1, 2, 3, 4};

    data_initialize(output, test_bytes, N_ENCODED_BYTES, 0);

    Data *decoded_data = decode_data(output, N_BITS_PER_PACKET);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, decoded_data, "The decoded data should not be null.");
    TEST_ASSERT_EQUAL(N_EXPECTED_BYTES, get_byte_count(decoded_data));
    TEST_ASSERT_EQUAL(N_EXPECTED_BITS, get_bit_count(decoded_data));
    TEST_ASSERT_EQUAL_UINT8_ARRAY(decoded_test_bytes, get_bytes(decoded_data), N_EXPECTED_BYTES);

    data_dispose(decoded_data);
}

void test_should_decode_data_in_bits(void) {

    #undef N_BITS_PER_PACKET
    #undef N_ENCODED_BYTES
    #undef N_EXPECTED_BYTES
    #undef N_EXPECTED_BITS
    #define N_BITS_PER_PACKET 8
    #define N_ENCODED_BITS 56
    #define N_ENCODED_BYTES 7
    #define N_EXPECTED_BYTES 4
    #define N_EXPECTED_BITS 28

    uint8_t test_bytes[N_ENCODED_BYTES] = {0, 0b01101001, 0, 0b10101010, 0, 0b11000011, 0};
    uint8_t decoded_test_bytes[N_EXPECTED_BYTES] = {1, 2, 3, 0};

    data_initialize(output, test_bytes, 0, N_ENCODED_BITS);

    Data *decoded_data = decode_data(output, N_BITS_PER_PACKET);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, decoded_data, "The decoded data should not be null.");
    TEST_ASSERT_EQUAL(N_EXPECTED_BYTES, get_byte_count(decoded_data));
    TEST_ASSERT_EQUAL(N_EXPECTED_BITS, get_bit_count(decoded_data));

    // Every byte up until the last one should match exactly.
    TEST_ASSERT_EQUAL_UINT8_ARRAY(decoded_test_bytes, get_bytes(decoded_data), N_EXPECTED_BYTES - 1);

    // Only the first 4 most significant bits need to match.
    TEST_ASSERT_EQUAL(decoded_test_bytes[N_EXPECTED_BYTES - 1],
                      get_bytes(decoded_data)[N_EXPECTED_BYTES - 1] & 0b11110000);

    data_dispose(decoded_data);
}

void test_should_decode_corrupted_data_in_bits(void) {

    #undef N_BITS_PER_PACKET
    #undef N_ENCODED_BYTES
    #undef N_EXPECTED_BYTES
    #undef N_EXPECTED_BITS
    #define N_BITS_PER_PACKET 8
    #define N_ENCODED_BITS 56
    #define N_ENCODED_BYTES 7
    #define N_EXPECTED_BYTES 4
    #define N_EXPECTED_BITS 28

    // Uncorrupted Data:                  {0, 0b01101001, 0, 0b10101010, 0, 0b11000011, 0};
    uint8_t test_bytes[N_ENCODED_BYTES] = {8, 0b00101001, 1, 0b00101010, 2, 0b11001011, 4};
    uint8_t decoded_test_bytes[N_EXPECTED_BYTES] = {1, 2, 3, 0};

    data_initialize(output, test_bytes, 0, N_ENCODED_BITS);

    Data *decoded_data = decode_data(output, N_BITS_PER_PACKET);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, decoded_data, "The decoded data should not be null.");
    TEST_ASSERT_EQUAL(N_EXPECTED_BYTES, get_byte_count(decoded_data));
    TEST_ASSERT_EQUAL(N_EXPECTED_BITS, get_bit_count(decoded_data));

    // Every byte up until the last one should match exactly.
    TEST_ASSERT_EQUAL_UINT8_ARRAY(decoded_test_bytes, get_bytes(decoded_data), N_EXPECTED_BYTES - 1);

    // Only the first 4 most significant bits need to match.
    TEST_ASSERT_EQUAL(decoded_test_bytes[N_EXPECTED_BYTES - 1],
                      get_bytes(decoded_data)[N_EXPECTED_BYTES - 1] & 0b11110000);

    data_dispose(decoded_data);
}
