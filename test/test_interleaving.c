#include "unity.h"
#include "data.h"
#include "interleaving.h"

Data *output;

void setUp(void) {
    output = data_create();
}

void tearDown(void) {
    data_dispose(output);
}

void test_should_not_change_data_packet_by_interleaving(void) {

    #undef N_TEST_BYTES
    #undef N_TEST_BITS
    #undef N_GROUPS
    #define N_TEST_BYTES 2
    #define N_TEST_BITS 16
    #define N_GROUPS 1

    uint8_t test_bytes[N_TEST_BYTES] = {10, 23};

    data_initialize(output, test_bytes, N_TEST_BYTES, 0);

    Data *interleaved_data = interleave_data(output, N_GROUPS);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, interleaved_data, "The interleaved data must not be null.");
    TEST_ASSERT_EQUAL(get_bit_count(interleaved_data), N_TEST_BITS);
    TEST_ASSERT_EQUAL(get_byte_count(interleaved_data), N_TEST_BYTES);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(test_bytes, get_bytes(interleaved_data), N_TEST_BYTES);

    data_dispose(interleaved_data);
}

void test_should_interleave_data_packet_in_bytes(void) {

    #undef N_TEST_BYTES
    #undef N_TEST_BITS
    #undef N_GROUPS
    #define N_TEST_BYTES 2
    #define N_TEST_BITS 16
    #define N_GROUPS 2

    uint8_t test_bytes[N_TEST_BYTES] = {0b11111111, 0b00000000};
    uint8_t expected_bytes[N_TEST_BYTES] = {0b10101010, 0b10101010};

    data_initialize(output, test_bytes, N_TEST_BYTES, 0);

    Data *interleaved_data = interleave_data(output, N_GROUPS);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, interleaved_data, "The interleaved data must not be null.");
    TEST_ASSERT_EQUAL(get_bit_count(interleaved_data), N_TEST_BITS);
    TEST_ASSERT_EQUAL(get_byte_count(interleaved_data), N_TEST_BYTES);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, get_bytes(interleaved_data), N_TEST_BYTES);

    data_dispose(interleaved_data);
}

void test_should_interleave_data_packet_in_bits(void) {

    #undef N_TEST_BYTES
    #undef N_TEST_BITS
    #undef N_GROUPS
    #define N_TEST_BYTES 2
    #define N_TEST_BITS 12
    #define N_GROUPS 2

    uint8_t test_bytes[N_TEST_BYTES] = {0b11111111, 0b00000000};
    uint8_t expected_bytes[N_TEST_BYTES] = {0b11111010, 0b10100000};

    data_initialize(output, test_bytes, 0, N_TEST_BITS);

    Data *interleaved_data = interleave_data(output, N_GROUPS);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, interleaved_data, "The interleaved data must not be null.");
    TEST_ASSERT_EQUAL(get_bit_count(interleaved_data), N_TEST_BITS);
    TEST_ASSERT_EQUAL(get_byte_count(interleaved_data), N_TEST_BYTES);

    // All bytes until the last should be exactly the same.
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, get_bytes(interleaved_data), N_TEST_BYTES - 1);

    // Only the bits that are part of the data need to be the same (the rest may not).
    TEST_ASSERT_EQUAL(expected_bytes[N_TEST_BYTES - 1],
                      get_bytes(interleaved_data)[N_TEST_BYTES - 1] & 0b11110000);

    data_dispose(interleaved_data);
}

void test_should_not_change_data_packet_by_reverting(void) {

    #undef N_TEST_BYTES
    #undef N_TEST_BITS
    #undef N_GROUPS
    #define N_TEST_BYTES 2
    #define N_TEST_BITS 16
    #define N_GROUPS 1

    uint8_t test_bytes[N_TEST_BYTES] = {10, 23};

    data_initialize(output, test_bytes, N_TEST_BYTES, 0);

    Data *reverted_data = revert_data(output, N_GROUPS);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, reverted_data, "The reverted data must not be null.");
    TEST_ASSERT_EQUAL(get_bit_count(reverted_data), N_TEST_BITS);
    TEST_ASSERT_EQUAL(get_byte_count(reverted_data), N_TEST_BYTES);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(test_bytes, get_bytes(reverted_data), N_TEST_BYTES);

    data_dispose(reverted_data);
}

void test_should_revert_data_packet_in_bytes(void) {

    #undef N_TEST_BYTES
    #undef N_TEST_BITS
    #undef N_GROUPS
    #define N_TEST_BYTES 2
    #define N_TEST_BITS 16
    #define N_GROUPS 2

    uint8_t test_bytes[N_TEST_BYTES] = {0b10101010, 0b10101010};
    uint8_t expected_bytes[N_TEST_BYTES] = {0b11111111, 0b00000000};

    data_initialize(output, test_bytes, N_TEST_BYTES, 0);

    Data *reverted_data = revert_data(output, N_GROUPS);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, reverted_data, "The reverted data must not be null.");
    TEST_ASSERT_EQUAL(get_bit_count(reverted_data), N_TEST_BITS);
    TEST_ASSERT_EQUAL(get_byte_count(reverted_data), N_TEST_BYTES);
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, get_bytes(reverted_data), N_TEST_BYTES);

    data_dispose(reverted_data);
}

void test_should_revert_data_packet_in_bits(void) {

    #undef N_TEST_BYTES
    #undef N_TEST_BITS
    #undef N_GROUPS
    #define N_TEST_BYTES 2
    #define N_TEST_BITS 12
    #define N_GROUPS 2

    uint8_t test_bytes[N_TEST_BYTES] = {0b11111010, 0b10100000};
    uint8_t expected_bytes[N_TEST_BYTES] = {0b11111111, 0b00000000};

    data_initialize(output, test_bytes, 0, N_TEST_BITS);

    Data *reverted_data = revert_data(output, N_GROUPS);

    TEST_ASSERT_NOT_EQUAL_MESSAGE(NULL, reverted_data, "The reverted data must not be null.");
    TEST_ASSERT_EQUAL(get_bit_count(reverted_data), N_TEST_BITS);
    TEST_ASSERT_EQUAL(get_byte_count(reverted_data), N_TEST_BYTES);

    // All bytes until the last should be exactly the same.
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expected_bytes, get_bytes(reverted_data), N_TEST_BYTES - 1);

    // Only the bits that are part of the data need to be the same (the rest may not).
    TEST_ASSERT_EQUAL(expected_bytes[N_TEST_BYTES - 1],
                      get_bytes(reverted_data)[N_TEST_BYTES - 1] & 0b11110000);

    data_dispose(reverted_data);
}
